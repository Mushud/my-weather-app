package sample;

import javafx.application.Application;

import javafx.fxml.FXMLLoader;

import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.net.*;


public class Main extends Application {

    private TextField searchFieldCity;
    private Button searchBtn;
    private Label outputTxt;
    private GridPane resultsPane;
    private Label intro;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setResizable(false);


       initialiseViews(root);

       initialiseMenuBar(root, primaryStage);

        searchBtn.setOnAction(e -> {
            String city = searchFieldCity.getText();
            if(city.isEmpty()){
                    Alert infoAlert = new Alert(Alert.AlertType.WARNING);
                    infoAlert.setTitle("Empty Field");
                    infoAlert.setHeaderText("City Field cannot be Empty");
                    infoAlert.showAndWait();
            }else {
                String url = "http://api.openweathermap.org/data/2.5/weather?q="+city +"&appid=be34bce58d100edcccbd5177b4b4dc89";
                try {
                    intro = (Label) root.lookup("#timeInfo");

                    JSONObject weatherJSONObject = requestWeather(url);
                    System.out.println(weatherJSONObject.toString());

                    JSONArray weatherArray = weatherJSONObject.getJSONArray("weather");
                    String weatherDescription;
                    String weatherMain;
                    String iconID;

                    JSONObject arrayObject =(JSONObject) weatherArray.get(0);
                    weatherDescription = arrayObject.getString("description");
                    weatherMain = arrayObject.getString("main");
                    iconID = arrayObject.getString("icon");

                    javafx.scene.image.ImageView weatherImage = (ImageView) root.lookup("#detailsImage");
                    weatherImage.setImage(new Image("/png/" + iconID +".png"));

                    Label weatherMainLabel = (Label) root.lookup("#weatherMain");
                    weatherMainLabel.setText(weatherMain + " \n" + weatherDescription);

                    Label cityDetails = (Label) root.lookup("#cityDetails");
                    JSONObject systemObject = weatherJSONObject.getJSONObject("sys");
                    cityDetails.setText(weatherJSONObject.getString("name") + ", " + systemObject.getString("country"));

                    Label temperatureLabel = (Label) root.lookup("#temperature");
                    JSONObject tempObject = weatherJSONObject.getJSONObject("main");
                    temperatureLabel.setText(String.valueOf(tempObject.getDouble("temp")));


                    Label windSpeedLabel = (Label) root.lookup("#windSpeed");
                    Label windDegress = (Label) root.lookup("#windDegrees");

                    JSONObject windObject = weatherJSONObject.getJSONObject("wind");
                   windSpeedLabel.setText("Speed: "+ windObject.getDouble("speed"));
                   windDegress.setText("Degrees: " + windObject.getDouble("deg"));

                   JSONObject coordinate = weatherJSONObject.getJSONObject("coord");
                   Label latitude = (Label) root.lookup("#Latitude");
                   Label longitude = (Label) root.lookup("#Longitude");

                   latitude.setText("Lat: " + coordinate.getDouble("lat"));
                   longitude.setText("Long: " + coordinate.getDouble("lon"));


                } catch (UnknownHostException netException) {
                    Alert networkErrorAlert = new Alert(Alert.AlertType.ERROR);
                    networkErrorAlert.setTitle("Can't Connect to Internet");
                    networkErrorAlert.setHeaderText("Please connect pc to Internet!!");
                    networkErrorAlert.showAndWait();
                } catch (ConnectException conException){
                    Alert networkErrorAlert = new Alert(Alert.AlertType.ERROR);
                    networkErrorAlert.setTitle("Connection Timeout");
                    networkErrorAlert.setHeaderText("Error getting data, check Internet connection!");
                    networkErrorAlert.showAndWait();
                }
                catch (FileNotFoundException f){
                    Alert infoAlert = new Alert(Alert.AlertType.WARNING);
                    infoAlert.setTitle("Invalid City Name");
                    infoAlert.setHeaderText("Cannot Retrieve Weather info for city name " + city + "\n Try Again");
                    searchFieldCity.setText("");
                    infoAlert.showAndWait();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });



        primaryStage.setScene(new Scene(root));
        primaryStage.setTitle("Weather App");

        primaryStage.show();
    }

    private void initialiseMenuBar(Parent root, Stage primaryStage) {
        Menu menu = new Menu("File");
        Menu menu2 = new Menu("Help");

        MenuItem item1 = new MenuItem("About");
        MenuItem item2 = new MenuItem("Exit");

        menu2.getItems().add(item1);
        menu.getItems().add(item2);

        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().add(menu);
        menuBar.getMenus().add(menu2);

        VBox menuVbox = (VBox) root.lookup("#menuVBox");
        menuVbox.getChildren().add(menuBar);
        item2.setOnAction(e -> {
            primaryStage.close();
        });
        item1.setOnAction(e -> {
            Alert info = new Alert(Alert.AlertType.INFORMATION);
            info.setTitle("App Info");
            info.setHeaderText("My Weather App, Mush.Lab");
            info.showAndWait();
        });
    }


    public static void main(String[] args) {
        launch(args);
    }

    public  JSONObject requestWeather(String url) throws Exception{
        //intro.setText("Loading...");
        URL apiUrl = new URL(url);
        HttpURLConnection httpURLConnection = (HttpURLConnection) apiUrl.openConnection();
        httpURLConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
        httpURLConnection.setRequestProperty("Server", "openresty/1.9.7.1");
        httpURLConnection.setRequestMethod("GET");

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));


        String inputLine;

        StringBuilder response = new StringBuilder();
        while ((inputLine = bufferedReader.readLine()) != null){
            response.append(inputLine);
        }

        bufferedReader.close();


        return  new JSONObject(response.toString());
    }

    public void initialiseViews(Parent root){
        searchBtn = (Button) root.lookup("#searchBtn");
        searchFieldCity = (TextField) root.lookup("#inputFieldCity");
        outputTxt = (Label) root.lookup("#outputText");
        outputTxt.setAlignment(Pos.CENTER);
        resultsPane = (GridPane) root.lookup("#resultsPane");

    }

}






